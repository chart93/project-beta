# CarCar

Team:

* Charlie - Service microservice and automobile front-end
* Angel - Sales microservice and automobile front-end

## Design
To run the project all main pages are accessed by the tap bar on top. Clicking on each individual tab loads a new page with the tabs information as well as links providing ways to create, delete, and search.


![Project diagram](Project_beta_CarCar.png)


Urls:

For appointment GET and POST requests: http://localhost:8080/api/appointments/
For specific appointment GET, PUT, and DELETE requests: http://localhost:8080/api/appointments/id/

For technician GET and POST requests: http://localhost:8080/api/technicians/
For specific technician GET and DELETE requests: http://localhost:8080/api/technicians/employee_id/

## Service microservice

Explain your models and integration with the inventory
microservice, here.

The models I created were Technician, AutomobileVO, and Appointment. The technician model has a first_name charField, a last_name charField, and an employee_id integer field. The AutomobileVO has a vin and sold field both supposed to be linked via a poller file. The appointment model has a date_time field, reason charField, vin charField, a customer charField, a Status charField with default set to 'created', and a technician field with a foreign key linking too the technician model.

The Technician List displays technicians' first, last name, and employee ID. It includes a delete button next to each name. It also has a link to create a new technician. The create technician page will create and return you to the technician list when the button is pressed.

The service appointment list gives teh owner, vin, date_time, reason, tech name and id, vip status, actions to finish or cancel the appointment. All of this is ordered my vin and has a new appointment button to add a new appointment. When finished or canceled is pressed they label the appointment as such and remove it from the list. When making a new appointment the technician field is a dropdown menu with active technicians. The create button will return you to the appointment list page.

The automobiles list provides color, year, vin, sold status, and delete button. It also has a link to add a new automobile. When adding a new automobile the model is a dropdown of current models in database. The create button returns to the list.

The service history tab brings you to a complete list of appointments organized my vin with their status as either created, finished, or canceled. There is also a search bar to search by vin.


## Sales microservice


### Home Screen
![home](assets/home.png)


### Sales List

Select `Sales` menu to open the sales list page:
![Sales List](assets/SalesList.png)

Click in `Add New` to open the new sale form:

![New Sale Form](assets/CreateNewSale.png)


Select the sales person responsible for the sale:

![New Sale Form](assets/NewSalesChoosePerson.png)





### Sales Persons

Select `Sales Persons` menu to open the Sales Persons list page:

![Sales Persons List](assets/SalesPersonsList.png)

Click in `Add New` to open the sales person form:

![New Sales Person Form](assets/CreateNewSalesPerson.png)

Click in `History` to see the history of sales per Sales Person:

![Sales Persons History](assets/SalesHistoryBySalesPerson.png)


### Customers

Select `Customers` menu to open the Customers list page:

![Customers List](assets/CustomersList.png)

Click in `Add New` to open the create customer form:

![New Customer Form](assets/CreateNewCustomer.png)


