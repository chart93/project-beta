import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm () {
    const [appointmentData, setAppointmentData] = useState(
        {
          vin: '',
          customer: '',
          date_time: '',
          technician_employee_id: '',
          reason: '',
        })

    const [technicians, setTechnicians] = useState([])
    useEffect(() => {
        const fetchTechnicians = async () => {
            try {
                const url = 'http://localhost:8080/api/technicians/'
                const response = await fetch(url)
                if (!response.ok) {
                    throw new Error('Network response was not ok')
                }
                const data = await response.json()
                setTechnicians(data.technicians)
            } catch (error) {
                console.error('Failed to fetch technicians:', error)
            }
        }

        fetchTechnicians()
    }, [])


    const navigate = useNavigate()
    const handleSubmit = async (event) => {
        event.preventDefault()

        try {
            const url = 'http://localhost:8080/api/appointments/'
            const fetchConfig = {
                mode: 'cors',
                method: "post",
                body: JSON.stringify(appointmentData),
                headers: {
                    'Content-Type': 'application/json',
                },
              }
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
            setAppointmentData(
                {
                    vin: '',
                    customer: '',
                    date_time: '',
                    technician_employee_id: '',
                    reason: '',
                })
                navigate('/appointments')
            }else {
              throw new Error('Network response was not ok')
          }
        } catch (error) {
            console.error('Failed to fetch appointment:', error)
        }
    }

    const handleAppointmentChange = (event) => {
        const value = event.target.value
        const inputName =event.target.name
        setAppointmentData(
            {...appointmentData,
                [inputName]: value
            }
        )
    }


    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Appointment</h1>
          <form onSubmit={handleSubmit} id="add-appointment-form">
            <div className="form-floating mb-3">
              <input onChange={handleAppointmentChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAppointmentChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAppointmentChange} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
              <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="form-floating mb-3">
                <select onChange={handleAppointmentChange} required name="technician_employee_id" id="technician" className="form-control" value={appointmentData.technician_employee_id}>
                <option value="">Select Technician</option>
                    {technicians.map(technician => (
                        <option key={technician.employee_id} value={technician.employee_id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                    ))}
                </select>
                <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAppointmentChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default AppointmentForm
