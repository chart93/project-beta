import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function TechList() {
  const [techs, setTechs] = useState([])

    const getTechs = async () => {
        try {
            const url = 'http://localhost:8080/api/technicians/'
            const response = await fetch(url, {mode: 'cors'}) // adding this for cors {mode: "no-cors"}
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()
          setTechs(data.technicians)

      } catch (error) {
          console.error('Failed to fetch technicians:', error)
      }
    }


    const handleDelete= async (idx) => {
        try {
            const deleteTechnicianEmployeeId = techs[idx].employee_id;
            const url = `http://localhost:8080/api/technicians/${deleteTechnicianEmployeeId}/`;
            const response = await fetch(url, { method: 'DELETE' });
            if (response.ok) {
                // Remove the deleted technician from the techs
                setTechs(currentTechs => currentTechs.filter(technicians => technicians.employee_id !== deleteTechnicianEmployeeId));
            } else {
                throw new Error('Failed to delete employee')
            }
        } catch (error) {
          console.error('Error deleting employee:', error)
        }
    }

    useEffect(() => {
        getTechs()
    }, [])

    return (
        <div className="technician-List">
        <h1>Technician List</h1>
            <Link to="/technicians/new">Add New Technician</Link>
            <div className="technician-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        </tr>
                    </thead>
                <tbody>
                    {techs.map((technician, idx) => {
                        return (
                                <tr key={technician.employee_id}>
                                <td>{technician.employee_id}</td>
                                <td>{ technician.first_name }</td>
                                <td>{ technician.last_name }</td>
                                <td><button onClick={() => handleDelete(idx)} type="button">Delete</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
    export default TechList
