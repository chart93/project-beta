import React, { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';

// Function to add dot in the last three positions of a string of numbers
const addDotInLastThreePositions = str =>
    str.length > 3
      ? str.split('').slice(0, -3).join('') + '.' + str.slice(-3)
      : '0.' + str.padStart(2, '0');

function SalesHistory() {
    const [autoVOList, setAutoVOList] = useState([]); // Option tag variable
    const [salespersonList, setSalespersonList] = useState([]); 
    const [salesList, setSalesList] = useState([]);
    // keep a version of the original list
    const orgSalesList = useRef(null);
    const [customerList, setCustomerList] = useState([]);

    // Function to fetch AutoVO list
    const fetchAutoVO = async () => {
        try {
            const url = 'http://localhost:8090/api/autoVO/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAutoVOList(data.autoVO);
            } else {
                throw new Error('Failed to fetch AutoVO');
            }
        } catch (error) {
            console.error('Error fetching AutoVO:', error);
        }
    };

    // Function to fetch customer list
    const fetchCustomers = async () => {
        try {
            const url = 'http://localhost:8090/api/customers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setCustomerList(data.customer);
            } else {
                throw new Error('Failed to fetch customers');
            }
        } catch (error) {
            console.error('Error fetching customers:', error);
        }
    };

    // Function to fetch salesperson list
    const fetchSalespersons = async () => {
        try {
            const url = 'http://localhost:8090/api/salesperson/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalespersonList(data.salesperson);
            } else {
                throw new Error('Failed to fetch salespersons');
            }
        } catch (error) {
            console.error('Error fetching salespersons:', error);
        }
    };

    // Function to fetch sales list
    const fetchDataSales = async () => {
        try {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                orgSalesList.current = data.sales;
                setSalesList(data.sales);
            } else {
                throw new Error('Failed to fetch sales');
            }
        } catch (error) {
            console.error('Error fetching sales:', error);
        }
    };

    useEffect(() => {
        fetchCustomers(); // Fetch customer data when component mounts
        fetchSalespersons(); // Fetch salesperson data when component mounts
        fetchDataSales(); // Fetch sales data when component mounts
        fetchAutoVO(); // Fetch AutoVO data when component mounts
    }, []);

    const filter = (id) => {
        if (id === "-1") {
            setSalesList(orgSalesList.current)
        } else {
            const filteredList = orgSalesList.current.filter(({salesperson_id}) => salesperson_id === +id);
            setSalesList(filteredList);
        }
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        filter(value);
    };

    // Function to find salesperson by ID
    const findSalesPersonById = (id) => {
        return salespersonList.find(person => person.id === id);
    };

    // Function to find customer by ID
    const findCustomerById = (id) => {
        return customerList.find(customer => customer.id === id);
    };

    // Function to find AutoVO by ID
    const findAutoVOById = (id) => {
        return autoVOList.find(auto => auto.id === id);
    };

    const handlePrint = () => {
        window.print();
    };

    return (
        <div className="salespersons-List">
            <br />
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>                
            <Link to="/salesperson">👈🏼 Back</Link>
                <button onClick={handlePrint}>Print 🖨️</button>
            </div>           
            <br />
            <div className="salespersons-Detail">
                <form>
                    <div className="mb-3">
                        <label style={{ marginBottom: '6px'}}>Choose a sales person</label>
                        <select onChange={handleSalesPersonChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value="-1">All</option>
                            {salespersonList.map(salesperson => (
                                <option key={salesperson.id} value={salesperson.id}>{salesperson.last_name} {salesperson.first_name}</option>
                            ))}
                        </select>
                    </div>
                </form>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesList
                            .map((sale) => {
                                const salesperson = findSalesPersonById(sale.salesperson_id);
                                const customer = findCustomerById(sale.customer_id);
                                const autoVO = findAutoVOById(sale.automobile_id);
                                return (
                                    <tr key={sale.id}>
                                        <td>{salesperson ? `${salesperson.first_name} ${salesperson.last_name}` : '-'}</td>
                                        <td>{customer ? `${customer.first_name} ${customer.last_name}` : '-'}</td>
                                        <td>{autoVO ? autoVO.vin : '-'}</td>
                                        <td>${addDotInLastThreePositions(sale.price.toString())}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
export default SalesHistory;
