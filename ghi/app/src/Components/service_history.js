import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function ServiceHistory() {
  const [appointments, setAppointments] = useState([])
  const [allVins, setAllVins] = useState([])
  const [searchQuery, setSearchQuery] = useState('')

    const getAppointments = async () => {
        try {
            const url = 'http://localhost:8080/api/appointments/'
            const response = await fetch(url)
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()
          const sortedAppointments = data.Appointments.sort((a,b) => a.vin.localeCompare(b.vin))
          setAppointments(sortedAppointments)

      } catch (error) {
          console.error('Failed to fetch appointments:', error)
      }
    }


    const getVins = async () => {
        try {
            const url = 'http://localhost:8080/api/all-vins/'
            const response = await fetch(url)
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()
          console.log("All VINs", data)
          setAllVins(data.vins)

      } catch (error) {
          console.error('Failed to fetch VINs:', error)
      }
    }


    useEffect(() => {
        getAppointments()
        getVins()
    }, [])

    const filteredAppointments = appointments.filter(appointment =>
        appointment.vin.toLowerCase().includes(searchQuery.toLowerCase())
      )


    return (
        <div className="appointment-List">
        <h1>Appointment List</h1>
            <Link to="/appointments/new">Add New Appointment</Link>
            <input
            type='text'
            placeholder="Search by VIN"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}/>
            <div className="appointment-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>VIN</th>
                        <th>Owner</th>
                        <th>Date and Time</th>
                        <th>Reason for Visit</th>
                        <th>Technician</th>
                        <th>VIP</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                <tbody>
                    {filteredAppointments.map((appointment) => {
                        return (
                                <tr key={ appointment.id }>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ appointment.date_time }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.technician.first_name } - { appointment.technician.employee_id }</td>
                                <td>{ allVins.includes(appointment.vin) ? 'Yes' : 'No' }</td>
                                <td>{ appointment.status }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
    export default ServiceHistory
