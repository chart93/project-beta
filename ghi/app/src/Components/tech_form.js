import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function TechForm () {
    const [technicianData, setTechnicianData] = useState(
        {
          employee_id: '',
          first_name: '',
          last_name: '',
        })

    const navigate = useNavigate()
    const handleSubmit = async (event) => {
        event.preventDefault()

        try {
            const url = 'http://localhost:8080/api/technicians/'
            const fetchConfig = {
                mode: 'cors',
                method: "post",
                body: JSON.stringify(technicianData),
                headers: {
                    'Content-Type': 'application/json',
                },
              }
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
            setTechnicianData(
                {
                    employee_id: '',
                    first_name: '',
                    last_name: '',
                })
                navigate('/technicians')
            }else {
              throw new Error('Network response was not ok')
          }
        } catch (error) {
            console.error('Failed to fetch technician:', error)
        }
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value
        const inputName =event.target.name
        setTechnicianData(
            {...technicianData,
                [inputName]: value
            }
        )
    }


    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Technician</h1>
          <form onSubmit={handleSubmit} id="add-technician-form">
            <div className="form-floating mb-3">
              <input onChange={handleTechnicianChange} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control"/>
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTechnicianChange} placeholder="First name" required type="text" name="first_name" id="last_name" className="form-control"/>
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTechnicianChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
              <label htmlFor="last_name">Last Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default TechForm
