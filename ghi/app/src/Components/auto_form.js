import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AutoForm () {
    const [automobileData, setAutomobileData] = useState(
        {
          color: '',
          year: '',
          vin: '',
          model_id: '',
        })

    const navigate = useNavigate()
    const handleSubmit = async (event) => {
        event.preventDefault()
        console.log('Submitting automobileData:', automobileData)

        try {
            const url = 'http://localhost:8100/api/automobiles/'
            const fetchConfig = {
                mode: 'cors',
                method: "post",
                body: JSON.stringify(automobileData),
                headers: {
                    'Content-Type': 'application/json',
                },
              }
            const response = await fetch(url, fetchConfig)
            const responseData = await response.json()
            if (response.ok) {
                setAutomobileData(
                    {
                    color: '',
                    year: '',
                    vin: '',
                    model_id: '',
                    })
                navigate('/automobiles')
            }else {
              throw new Error('Network response was not ok')
          }
        } catch (error) {
            console.error('Failed to fetch automobile:', error)
        }
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value
        const inputName =event.target.name
        setAutomobileData(
            {...automobileData,
                [inputName]: value
            }
        )
    }
    const [models, setModels] = useState([])
    useEffect(() => {
        const fetchModels = async () => {
            try {
                const url = 'http://localhost:8100/api/models/'
                const response = await fetch(url)
                if (!response.ok) {
                    throw new Error('Network response was not ok')
                }
                const data = await response.json()
                console.log('Fetched models:', data.models)
                setModels(data.models || [])
            } catch (error) {
                console.error('Failed to fetch models:', error)
            }
        }

        fetchModels()
    }, [])


    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Automobile</h1>
          <form onSubmit={handleSubmit} id="add-automobile-form">
            <div className="form-floating mb-3">
              <input onChange={handleAutomobileChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAutomobileChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"/>
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleAutomobileChange} placeholder="Year" required type="text" name="year" id="year" className="form-control"/>
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
                <select onChange={handleAutomobileChange} required name="model_id" id="model_id" className="form-control" value={automobileData.model_id}>
                <option value="">Select Model</option>
                    {models.map(model => (
                        <option key={model.id} value={model.id}>
                            {model.name}
                        </option>
                    ))}
                </select>
                <label htmlFor="model_id">Model</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}

export default AutoForm
