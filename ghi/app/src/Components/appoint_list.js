import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function AppointmentList() {
  const [appointments, setAppointments] = useState([])
  const [allVins, setAllVins] = useState([])

    const getAppointments = async () => {
        try {
            const url = 'http://localhost:8080/api/appointments/'
            const response = await fetch(url)
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()
          const createdAppointments = data.Appointments.filter(appointment => appointment.status === 'created')
          const sortedAppointments = createdAppointments.sort((a,b) => a.vin.localeCompare(b.vin))
          setAppointments(sortedAppointments)

      } catch (error) {
          console.error('Failed to fetch appointments:', error)
      }
    }


    const getVins = async () => {
        try {
            const url = 'http://localhost:8080/api/all-vins/'
            const response = await fetch(url)
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()
          console.log("All VINs", data)
          setAllVins(data.vins.map(v => v.vin))

      } catch (error) {
          console.error('Failed to fetch VINs:', error)
      }
    }


    const updateStatus= async (idx, status) => {
        try {
            const appointmentId = appointments[idx].id;
            const url = `http://localhost:8080/api/appointments/${appointmentId}/`;
            const response = await fetch(url, {
                method: 'PUT',
                headers:{'Content-Type': 'application/json'},
                body: JSON.stringify({ status }),
             })
            if (response.ok) {
                setAppointments(currentAppointments => currentAppointments.filter(appointment => appointment.id !== appointmentId));
            } else {
                throw new Error('Failed to update appointment')
            }
        } catch (error) {
          console.error('Error updating appointment:', error)
        }
    }

    useEffect(() => {
        getAppointments()
        getVins()
    }, [])


    return (
        <div className="appointment-List">
        <h1>Appointment List</h1>
            <Link to="/appointments/new">Add New Appointment</Link>
            <div className="appointment-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>VIN</th>
                        <th>Owner</th>
                        <th>Date and Time</th>
                        <th>Reason for Visit</th>
                        <th>Technician</th>
                        <th>VIP</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                <tbody>
                    {appointments.map((appointment, idx) => {
                        return (
                                <tr key={ appointment.id }>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ appointment.date_time }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.technician.first_name } - { appointment.technician.employee_id }</td>
                                <td>{ allVins.includes(appointment.vin) ? 'Yes' : 'No' }</td>
                                <td><button onClick={() => updateStatus(idx, 'finished')} type="button">Finished</button>
                                <button onClick={() => updateStatus(idx, 'canceled')} type="button">Canceled</button>
                                </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
    export default AppointmentList
