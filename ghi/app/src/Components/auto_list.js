import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function AutoList() {
  const [automobiles, setAutomobiles] = useState([])

    const getAutomobiles = async () => {
        try {
            const url = 'http://localhost:8100/api/automobiles/'
            const response = await fetch(url, {mode: 'cors'}) // adding this for cors {mode: "no-cors"}
            if (!response.ok) {
              throw new Error('Network response was not ok')
          }
          const data = await response.json()

          setAutomobiles(data.autos)

      } catch (error) {
          console.error('Failed to fetch automobiles:', error)
      }
    }


    const handleDelete= async (idx) => {
        try {
            const deleteAutomobileVin = automobiles[idx].vin;
            const url = `http://localhost:8080/api/technicians/${deleteAutomobileVin}/`;
            const response = await fetch(url, { method: 'DELETE' });
            if (response.ok) {
                setAutomobiles(currentAutomobiles => currentAutomobiles.filter(auto => auto.vin !== deleteAutomobileVin));
            } else {
                throw new Error('Failed to delete automobile')
            }
        } catch (error) {
          console.error('Error deleting automobile:', error)
        }
    }

    useEffect(() => {
        getAutomobiles()
    }, [])

    return (
        <div className="automobile-List">
        <h1>Automobile List</h1>
            <Link to="/automobiles/new">Add New Automobile</Link>
            <div className="automobile-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>Color</th>
                        <th>Year</th>
                        <th>VIN</th>
                        <th>Sold Status</th>
                        <th>Model</th>
                        </tr>
                    </thead>
                <tbody>
                    {automobiles.map((automobile, idx) => {
                        return (
                                <tr key={automobile.vin}>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.vin}</td>
                                <td>{ (automobile.sold) ? 'Yes' : 'No' }</td>
                                <td>{ automobile.model.name }</td>
                                <td><button onClick={() => handleDelete(idx)} type="button">Delete</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
    export default AutoList
