import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerListForm  from './Components/CustomerListForm'; //Customer form
import CustomerNewForm from './Components/CustomerNewForm';
import SalesListForm from './Components/SalesListForm'; //Sales Form
import SalesForm from './Components/SalesForm';
import SalesPersonListForm from './Components/SalesPersonListForm';//sales person Form
import SalesPersonNewForm from './Components/SalesPersonNewForm';
import SalesHistory from './Components/SalesHistoryForm'
import TechList from './Components/tech_list';
import TechForm from './Components/tech_form';
import AppointmentList from './Components/appoint_list';
import AppointmentForm from './Components/appoint_form';
import AutoForm from './Components/auto_form';
import AutoList from './Components/auto_list';
import ServiceHistory from './Components/service_history';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/customers" element={<CustomerListForm />} />
          <Route path="/customers/new" element={<CustomerNewForm />} />
          <Route path="/salesperson" element={<SalesPersonListForm />} />
          <Route path="/salesperson/new" element={<SalesPersonNewForm />} />
          <Route path="/salesperson/history" element={<SalesHistory/>} />
          <Route path="/sales" element={<SalesListForm/>} />
          <Route path="/sales/new" element={<SalesForm/>} />
          {/* <Route path="/manufacturers" element={<ManufactureForm/>} /> */}
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<TechList />} />
          <Route path="/technicians/new" element={<TechForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/new" element={<AppointmentForm />} />
          <Route path="/automobiles" element={<AutoList />} />
          <Route path="/automobiles/new" element={<AutoForm />} />
          <Route path="/service-history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
