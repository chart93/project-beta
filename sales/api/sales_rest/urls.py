from django.urls import path
# from . import views
from .views import (
    # api_list_automobile,  
    api_salesperson, 
    api_customer,  
    api_sales, 
    api_automobileVOlist,       
)

urlpatterns = [
    # path('automobile/', api_list_automobile, name='api_list_automobile'),
    # path('automobile/<int:pk>/', api_show_automobile, name='api_show_automobile'),
    # salesperson api
    path('salesperson/', api_salesperson, name='api_salesperson'),
    path('salesperson/<int:pk>/', api_salesperson, name='api_salesperson'),
    #Customer Api
    path('customers/', api_customer, name='api_customer'),
    path('customers/<int:pk>/', api_customer, name='api_customer'),
    # sales api
    path('sales/', api_sales, name='api_sales'),
    path('sales/<int:pk>/', api_sales, name='api_sales'),
    #automobileVO
    path('autoVO/', api_automobileVOlist, name='api_automobileVOlist'),
    path('autoVO/<int:pk>/', api_automobileVOlist, name='api_automobileVOlist'),
]