from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder
# Create your views here.


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
    ]



@require_http_methods(["GET", "POST"])
# @cors_headers
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
# @cors_headers
def show_technician(request, employee_id):
    if request.method == "DELETE":
        count,_=Technician.objects.filter(employee_id=employee_id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        try:
            technician = Technician.objects.get(employee_id=employee_id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status=400,
                )
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "date_time",
        "reason",
        "status",
        "vin",
        "technician",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
        # "status": StatusEncoder(),
    }


@require_http_methods(["GET", "POST"])
# @cors_headers
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"Appointments": list(appointments)},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            tech_id = content["technician_employee_id"]
            technician = Technician.objects.get(employee_id=tech_id)
            content["technician"] = technician
            del content["technician_employee_id"]
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
                )
        except Exception as e:
            response = JsonResponse(
                {"message": f"Could not create the appointment: {str(e)}"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
# @cors_headers
def show_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=id)
            if "status" in content:
                setattr(appointment, "status", content["status"])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin",
                  "sold",]


def get_vins(request):
    all_vins = list(AutomobileVO.objects.values('vin'))
    return JsonResponse(
        {'vins': all_vins},
        encoder=AutomobileVOEncoder,
        safe=False,
        )
