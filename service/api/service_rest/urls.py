from django.urls import path
from .views import list_technicians, show_technician, list_appointments, show_appointment, get_vins


urlpatterns = [
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<int:employee_id>/", show_technician, name="show_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", show_appointment, name="show_appointment"),
    path('all-vins/', get_vins, name='get_vins'),
    # path("appointments/<int:id>/<str:status>", set_appointment, name="set_appointment")
]
