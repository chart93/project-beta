from django.db import models
from django.urls import reverse
# Create your models here.


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField()

    class Meta:
        ordering = ('employee_id', "first_name", "last_name")

    def __str__(self):
        return f"{self.first_name} ({self.employee_id})"

    def get_url(self):
        return reverse("technician", kwargs={"id": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)




class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150)
    status = models.CharField(max_length=15, default='created')
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        )
    # status = models.ForeignKey(
    #     Status,
    #     related_name="appointments",
    #     on_delete=models.CASCADE,
    # )

    def get_url(self):
        return reverse("appointment", kwargs={"id": self.id})

    def __str__(self):
        return f"{self.customer} for {self.reason} with {self.technician.first_name}"
